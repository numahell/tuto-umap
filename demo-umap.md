# Création d'une carte 

## Initiation Cartographie / OpenData

19 février 2019

<a rel="license" href="https://creativecommons.org/publicdomain/zero/1.0/deed.fr">Creative Commons CC0</a>
----

## Objectif

Créer une carte comme celle-ci

[![](./captures/umap-00-carte.png)](http://umap.openstreetmap.fr/fr/map/tiers-lieux-toulousains_210845#13/43.6067/1.4477)

Notes:

Des notions et des ressources seront fournies tout au long de la démo.

---

## Pour commencer

----

### Créer une carte

Sur [umap.openstreetmap.fr](http://umap.openstreetmap.fr/)

- créer un compte
- créer une carte

Notes:

Il est conseillé de créer un compte sur OpenStreetMap qui permet d'y contribuer.

----

### Éditer les propriétés

- donner un nom pour la carte et enregistrer

  ![](./captures/umap-01-nom-carte.png)
  ![](./captures/umap-02-proprietes-carte.png)

----

### Forme par défaut
  
- choisir un icon par défaut

  ![](./captures/umap-03-proprietes-icones.png)

----

## Définir les limites géographiques

- chercher Toulouse, puis définir les limites

Coordonnées GPS utilisées 

- lattitude = 43,607835
- longitude = 1,435604

----

## Choisir un fond de carte

Style de la carte, ou comment les informations géographiques sont représentées.

  ![](./captures/umap-04-fond-carte.png)

Notes:

Dépend souvent des informations à mettre en avant.

---

## Ajouter des objets

----

### Les objets géographiques

Données géographiques : point, ligne, polygone

![](./captures/umap-05-ajout-edit-formes.png)

----

### Les formats de données

- données : csv, json, xml, …
- géographiques  : geojson (le plus courant), kml, gpx, shapefile

![Par exemple le geojson](./captures/umap-06-data-format-json.png)

---

## Importer des données

----

### Récupérer des données

Récupérer les [données des bibliothèques](https://data.toulouse-metropole.fr/explore/dataset/mediatheques-bibliotheques-et-bibliobus/information/) depuis le portail OpenData Toulouse au format geojson

----

### Les licences

- Les licences pour l'OpenData : [ODbL](https://opendatacommons.org/licenses/odbl/) (Open Database Licence), [Licence Ouverte](https://github.com/etalab/licence-ouverte/blob/master/LO.md)
- Les licences pour les fonds de carte : plutôt [Creative commons](http://creativecommons.fr/)

----

### Importer les données dans Umap

Importer les données des bibliothèques

  ![](./captures/umap-06-import-data.png)

---

## Autres fonctionnalités

----

### Gestion des calques

Ordre d'affichage, propriétés spécifiques de la forme ou intéractions

![](./captures/umap-07-liste-calques.png)

----

### Intéractions

Affichage de popup avec informations suplémentaires

![](./captures/umap-08-popup-data.png)
![](./captures/umap-08-popup-affichage.png)

----

### Visualisation

- clusters (regroupement de points)
- heatmap (représentation d'une densité, épaisseur ou couleur pondérée)

Ex: cluster

![Exemple affichage de cluster](./captures/umap-08-affichage-cluster.png)

----

### Droits d'édition

Partage de l'édition de la carte à d'autres personnes connectées ou à tout le monde

![](./captures/umap-08-gestion-droits-modif.png)

---

## Ressources

----

### Umap

- wiki OpenStreetMap [wiki.openstreetmap.org/wiki/UMap](https://wiki.openstreetmap.org/wiki/UMap)
- Tutoriels en français [wiki.cartocite.fr](https://wiki.cartocite.fr/doku.php?id=umap:tutoriel_umap)

----

### OpenStreetMap, la cartographie libre

- Données géographiques libres, nombreux contributeurs⋅ices
- Affichage de cartes, itinéraire, contribution (édition des données)

[openstreetmap.org](http://openstreetmap.org/)

----

### OpenData, données ouvertes

- Licences permettant la réutilisation des données (ODbL, Licence Ouverte)
- Format utilisable pour des traitements automatisés
- Portails de données, par exemple
  - [data.gouv.fr](https://www.data.gouv.fr/fr/)
  - [data.toulouse-metropole.fr](https://data.toulouse-metropole.fr/pages/accueil/)
- Structures institutionnelles de l'OpenData en France
  - [www.opendatafrance.net](http://www.opendatafrance.net/)
  - [etalab.gouv.fr](https://www.etalab.gouv.fr/)

----

### Les Communs

- Site web présentation [lescommuns.org](http://lescommuns.org/) → [Outils](http://lescommuns.org/les-outils/)
- Blog [les-communs-dabord.org](http://les-communs-dabord.org/)
- Outils de collaboration : contactez des acteurs⋅ices des Communs
  et posez vos questions sur le [forum](https://forum.lescommuns.org/),
  la [liste de discussion](https://listes.cfcloud.fr/bienscommuns.org/subscribe/echanges)
  ou le [chat](https://chat.lescommuns.org/home)
- Voir aussi [remixthecommons.org](https://wiki.remixthecommons.org/)

----

### Transiscope, données des alternatives

- Une carte qui agrège les alternatives
- Plusieurs acteurs, autant de sources de données : Colibris, Alternatiba, CRID, RIPESS, MES, …

[transiscope.org](https://transiscope.org/)

![Transiscope centré sur Toulouse](./captures/transiscope.png)

---

## Bonus: Logiciels Libres et Communs numériques

----

### Promotion du libre, sensibilisation aux enjeux, Internet et libertés

- [framasoft.org](https://framasoft.org/), association d'éducation populaire pour un numérique libre
- [april.org](https://www.april.org/), association pour la promotion du Logiciel Libre
- La Quadrature du Net, défense des droits et libertés fondamentales à l’ère du numérique
[laquadrature.net](https://www.laquadrature.net/)

----

### Outils libres, hébergement web alternatif, accès internet associatif

- Outils en ligne libres [degooglisons-internet.org](http://degooglisons-internet.org/)
- Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires [chatons.org](https://chatons.org/)
- La Fédération FDN, Fédération des fournisseurs d’accès à internet associatifs [ffdn.org](https://www.ffdn.org/)

----

### Culture libre, OpenData

- [Wikipedia](https://fr.wikipedia.org/), l'associations [Wikimedia France](https://www.wikimedia.fr/)
- [OpenStreetMap](http://openstreetmap.org/), l'association [OpenStreetMap France](http://www.openstreetmap.fr/)
- [SavoirsCom1](http://www.savoirscom1.info/), collectif pour le développement des communs de la connaissance
- [Portail OpenData](https://www.data.gouv.fr/) au niveau national, [Etalab](https://www.etalab.gouv.fr/), [OpenData France](http://www.opendatafrance.net/) (plus institutionnels)

----

### À Toulouse

- Aide à l'utilisation [toulibre.org](http://toulibre.org/)
- Fournisseur d'accès Internet [tetaneutral.net](http://www.tetaneutral.net/) et membre de la FFDN
- Hackerspace [tetalab.org](https://www.tetalab.org/)
- Fablabs : Campus Fab, le Ricquet, Articlect
- Médiation numérique [combustible.fr](http://combustible.fr/)
- etc.

---

## Bonus 2: cartes de Tiers-Lieux

(Ajout du 3/03/19)

----

### Par territoire

- [Lille](http://hauts.tiers-lieux.org/mel/)
- [Aquitaine](https://coop.tierslieux.net/le-reseau/carte/) (carte Umap)
- [Occitanie](http://www.tierslieuxoccitanie.com/working_places) (carte Google Maps)
- [Tarn](http://coworking-tarn.com/#lestierslieux)
- [Gers](http://plage.kopane.fr/wakka.php?wiki=LesLieux)
- [Hérault](http://geo.herault.fr/numerique_inclusif/index.html)
- [Pays de Loire](http://www.parcoursnumeriques.net/carte/)
- [Île et Vilaine](http://www.audiar.org/node/449?#) (pdf)

----

### Par types de lieux

- [Tiers-lieux open source](http://tiers-lieux.org/carte/) (et le [wiki](http://movilab.org/index.php?title=Accueil))
- [Fablabs](https://www.fablabs.io/labs/map)
- [tous les labs](http://www.makery.info/labs-map/) par Makery…

---

# Merci