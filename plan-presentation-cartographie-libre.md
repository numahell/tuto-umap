% Initiation : afficher des données sur une carte
% Emmanuelle Helly

# Initiation : afficher des données sur une carte

Science Po – 19 février 2019

## Objectif

Créer une carte comme celle-ci

[http://umap.openstreetmap.fr/fr/map/tiers-lieux-toulousains_210845#13/43.6067/1.4477](http://umap.openstreetmap.fr/fr/map/tiers-lieux-toulousains_210845#13/43.6067/1.4477)

Des notions et des ressources seront fournies tout au long de la démo.

## Quelques notions

### Les données

- formats de données : json, xml, csv
- formats de données avec coordonnées géographiques : geojson, shapefile, gpx, kml
- données géographiques : point, lignes ou polygones
- licences ouvertes : réutilisation autorisée

### Les cartes

Coordonnées, projections, couches et fonds de carte

## Ressources OpenData

### OpenStreetMap, cartographie libre

- données géographiques en OpenData
- affichage sous forme de carte → fonctionnalités de recherche, itinéraire, signaler une erreur, édition (sous réserve de créer un compte)

### Les portails OpenData

- data.gouv.fr
- portails régionaux, métropoles, etc.

→ vidéo Sandrine Mathon

## Les outils

### Afficher des données sur une carte

Démo de Umap

[http://umap.openstreetmap.fr/](http://umap.openstreetmap.fr/)

### Autres outils libres

De nombreux outils en ligne libres proposés sur [degooglisons-internet.org](https://degooglisons-internet.org/fr/)

Pour communiquer, échanger, s'organiser, écrire à plusieurs… 
